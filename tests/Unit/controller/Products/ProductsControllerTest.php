<?php

namespace Tests\Unit\controller\Products;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;

class ProductsControllerTest extends TestCase
{
    public function test_get_all_productions()
    {
        $payLoad = 20;
        $response = $this->get('/api/products/view/all',['perPage' => $payLoad]);

        $response->assertStatus(200);
    }
}
