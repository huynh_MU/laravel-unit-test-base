## Laravel CRUD API with Auth
Basic Laravel CRUD API application

### Language & Framework Used:
1. PHP-8.1
2. Laravel-9.2

### Architecture Used:
1. Laravel 9.x
2. Interface-Repository Pattern
3. Model Based Eloquent Query
4. PHP Unit Testing - Some basic unit testing added.

### How to Run:
1. Clone Project
2. Go to the project drectory
3. Create `.env` file & Copy `.env.example` file to `.env` file
4. Create a new database
6. Install composer packages - `composer install`.
6. Now migrate and seed database to complete whole project setup by running this-
``` bash
php artisan migrate:refresh --seed
```
7. Generate Swagger API
``` bash
php artisan l5-swagger:generate
```
8. Run the server -
``` bash
php artisan serve
```
9. Open Browser -
http://127.0.0.1:8000 & go to API Documentation -
http://127.0.0.1:8000/api/documentation
10. You'll see a Swagger Panel.

### Unit Test
1. Install X-debug
2. Config files phpunit.xml
3. Run : ./vendor/bin/phpunit to run unit test (all file)
4. Run all tests:
``` bash
./vendor/bin/phpunit
```
5. Run testsuite:
``` bash
./vendor/bin/phpunit --testsuite Unit
```
6. Run file:
``` bash
./vendor/bin/phpunit tests/Unit/ExampleTest.php
```
7. Run Format output:
``` bash
./vendor/bin/phpunit --testdox
```
8. Create Coverage Reports and CRAP
``` bash
./vendor/bin/phpunit --coverage-html location_folder
```
